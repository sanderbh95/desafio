<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/** 
* @OA\Info( 
*   title="DOCUMENTAÇÃO DESAFIO API", 
*   version="1.0.0",
*   @OA\Contact(
*       email="sanderbh95@gmail.com"
*   )
* ) 
*/class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
