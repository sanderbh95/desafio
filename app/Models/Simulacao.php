<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class Simulacao extends Model
{
    use HasFactory;

    /**
     * Retornando as taxas do arquivo taxas_instituicoes.json.
     */
    public function getTaxas(){
        try{
            $file = file_get_contents(public_path('simulador/taxas_instituicoes.json'));
            $taxas = json_decode($file);
            return $taxas;
        }catch(\Exception $e){
            return $e;
        }
    }

    /**
     * Ordenando Taxas por parcela.
     */
    public function ordenarTaxasParcela($taxas){
        $dados = $taxas;
        usort(
            $dados,
            function( $a, $b ){
                if($a->parcelas == $b->parcelas){
                    return 0;
                }
                return ($a->parcelas < $b->parcelas) ? -1 : 1;
            }
        );

        return $dados;
    }

    /**
     * Filtrando as taxas por instituicao, convenio, parcela.
     */
    public function filtroTaxas($instituicao, $convenio, $parcelas = null){
        $taxas = $this->getTaxas();
        $filtro = array_filter(
            $taxas, 
            function ($it) use ($instituicao, $convenio, $parcelas){ 
                if(is_null($parcelas) == false){
                    return $it->instituicao == $instituicao && $it->convenio == $convenio && $it->parcelas == $parcelas;
                }else{
                    return $it->instituicao == $instituicao && $it->convenio == $convenio;
                }
            }
        );
        $taxasOrdenador = $this->ordenarTaxasParcela($filtro);

        return $taxasOrdenador;
    }

     /**
     * Calculando o valor da parcela.
     */
    public function calculaValorParcela($valorEmprestimo, $coeficiente){
        $valor = floatval(number_format((floatval($valorEmprestimo) * floatval($coeficiente)), 2));

        return $valor;
    }
    
    /**
     * Criando simulação do cliente.
     */
    public function simulacaoCliente($request){
        try{
            $instituicaoClass = new Instituicao();
            $convenioClass = new Convenio();
            
            $instituicao = $request->instituicoes ? array_unique($request->instituicoes) : $instituicaoClass->getChaveInstituicao();
            $convenio =$request->convenios ? array_unique($request->convenios) : $convenioClass->getChaveConvenio();
            $parcela = $request->parcela ? $request->parcela : null;
            $valorEmprestimo = $request->valor_emprestimo;

            $object = new stdClass;
            foreach($instituicao as $inst){
                $taxaInstituicao = [];
                foreach($convenio as $conv){
                    $filtroTaxa = $this->filtroTaxas($inst, $conv, $parcela);
                    $arrayTaxa = [];
                    foreach($filtroTaxa as $tax){
                        $obj = new stdClass;
                        $obj->taxa = $tax->taxaJuros;
                        $obj->parcelas = $tax->parcelas;
                        $obj->valor_parcela = $this->calculaValorParcela($valorEmprestimo, $tax->coeficiente);
                        $obj->convenio = $tax->convenio;
                        array_push($arrayTaxa, $obj);
                    }
                    $aux = array_merge($taxaInstituicao, $arrayTaxa);
                    $taxaInstituicao = $aux;
                }
                $object->$inst = $taxaInstituicao;
            }
            return $object;
        }catch(\Exception $e){
            return response()->json(["error" => "Não foi possível realizar a simulação. Contate o suporte"], 404);
        }
    }

    /**
     * Regras de validação de cada campo 
     */
    protected function rules()
    {
        return [
            'valor_emprestimo' => 'required|numeric|min:0',
            'instituicoes' => 'array|nullable',
            'convenios' => 'array|nullable',
            'parcela' => 'integer|nullable'
        ];
    }

    /**
     * Mensagens de retorno de validações
     */
    protected function customMessages()
    {
        return [
            'valor_emprestimo.required' => 'O campo valor do empréstimo é obrigatório',
            'valor_emprestimo.numeric' => 'O campo valor do empréstimo deve ser numérico',
            'valor_emprestimo.min' => 'O campo valor do empréstimo tem um valor mínimo de 0',
            'instituicoes.array' => 'O campo instituições deve ser do tipo array',
            'convenios.array' => 'O campo convênios deve ser do tipo array',
            'parcela.integer' => 'O campo parcelas deve ser número inteiro'
        ];
    }

    /**
     * Validando os campos 
     */
    public function customValidate(Request $request)
    {
        $validator = Validator::make( $request->all(), $this->rules(), $this->customMessages());

        if($validator->fails()){
            return $validator->errors();
        }else{
            return ;
        }
    }
}
