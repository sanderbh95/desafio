<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{
    use HasFactory;

    public function getInstituicao(){
        try{
            $file = file_get_contents(public_path('simulador/instituicoes.json'));
            $instituicao = json_decode($file);
            
            return $instituicao;
        }catch(\Exception $e){
            return $e;
        }
    }

    public function getChaveInstituicao(){
        try{
            $instituicao = $this->getInstituicao();
            $valores = [];
            foreach($instituicao as $value){
                $valores[] = $value->chave;
            }
            
            return $valores;
        }catch(\Exception $e){
            return $e;
        }
    }
}
