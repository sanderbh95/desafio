<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
    use HasFactory;
    
    /**
     * Retornando convenios do arquivo convenios.json.
     */
    public function getConvenio(){
        try{
            $file = file_get_contents(public_path('simulador/convenios.json'));
            $convenio = json_decode($file);
            return $convenio;
        }catch(\Exception $e){
            return $e;
        }
    }

     /**
     * Retornando apenas as chaves dos convênios do arquivo convenios.json.
     */
    public function getChaveConvenio(){
        try{
            $convenio = $this->getConvenio();
            $valores = [];
            foreach($convenio as $value){
                $valores[] = $value->chave;
            }
            
            return $valores;
        }catch(\Exception $e){
            return $e;
        }
    }
}
